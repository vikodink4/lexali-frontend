import Vue from 'vue'
import App from './App.vue'
import './components'
import router from './router/router'
import {store} from './store/store'

import axios from 'axios'
import VueRouter from 'vue-router'
import Vuetify from 'vuetify'


Vue.use(axios, VueRouter, Vuetify);

import 'vuetify/dist/vuetify.min.css'

new Vue({
  store,
  router,
  el: '#app',
  render: h => h(App)
});
