import VueRouter from 'vue-router'
import Vue from 'vue'

import main from '../components/main/main.vue'
import products from '../components/products/products'
import productDetailed from '../components/product_detailed/productDetailed'

Vue.use(VueRouter);



export default new VueRouter ({
  mode: 'history',
  base: __dirname,
  routes: [
    // { path: '/cartoons/:id', component: Cartoon, name: 'cartoon'},
    { path: '/', component: main, name: 'main-page' },
    { path: '/products', component: products, name: 'products' },
    { path: '/products/:id', component: productDetailed, name: 'product' }
  ]
});
