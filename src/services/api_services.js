import axios from 'axios';

const localhostLink = 'http://localhost:8000';

// get all filters that can be checked or unchecked from a backend API
export function getAllCheckFilters () {return axios.get(localhostLink + '/shop/all_check_filters/')}

// get all filters that can have sliders
export function getPriceRangeForPriceFilter() {return axios.get(localhostLink + '/shop/price_range/')}

// get all products from a backend API
export function getAllProducts () {return axios.get(localhostLink + '/shop/all_products/')}

// get specific product from a backend API
export function getSpecificProduct (productId) {return axios.get(localhostLink + '/shop/products/' + productId)}


