import Vue from 'vue'
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    allProducts: [],
    allCategories: [],
    isShopSubMenuActive: false,
    isMainBackgroundBlackout: false,
    isCurrencyMenuActive: false

  },

  mutations: {
    setAllProducts: (state, products) => {
      for (let product of products) {
        state.allProducts.push(product);
      }
    },

    removeAllProducts: (state) => {
      state.allProducts = [];
    },

    setAllCategories: (state, categories) => {
      for (let category of categories) {
        state.allCategories.push(category);
      }
    },

    changeShopSubMenu: (state) => {
      state.isShopSubMenuActive = state.isShopSubMenuActive === false;
    },

    changeMainBackground: (state) => {
      state.isMainBackgroundBlackout = state.isMainBackgroundBlackout === false;
    },

    changeCurrencyMenu: (state) => {
      state.isCurrencyMenuActive = state.isCurrencyMenuActive === false;
    }
  },

  getters: {
    getAllProducts: state => {
      return state.allProducts;
    },

    getAllCategories: state => {
      return state.allCategories;
    },

    getShopSubMenuState: state => {
      return state.isShopSubMenuActive;
    },

    getMainBackgroundState: state => {
      return state.isMainBackgroundBlackout;
    },

    getCurrencyMenuState: state => {
      return state.isCurrencyMenuActive;
    }
  }
});
