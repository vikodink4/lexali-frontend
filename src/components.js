import Vue from 'vue'

// Base components
import footer from './components/base/footer/footer.vue'
import header from './components/base/header/header.vue'

Vue.component('main-footer', footer);
Vue.component('main-header', header);


// Main page
import main from './components/main/main.vue'
import mainImage from './components/main/parts/mainImage.vue'
import info from './components/main/parts/info.vue'
import productsButtonSection from './components/main/parts/productsButtonSection.vue'

Vue.component('main-page', main);
Vue.component('main-image', mainImage);
Vue.component('info', info);
Vue.component('products-button-section', productsButtonSection);

// Products
import products from './components/products/products'
import productFilter from './components/products/parts/productFilter'
import productGrid from './components/products/parts/productGrid'
import productSorting from './components/products/parts/productSorting'

Vue.component('products', products);
Vue.component('product-filter', productFilter);
Vue.component('product-grid', productGrid);
Vue.component('product-sorting', productSorting);

// Product detailed

import productDetailed from './components/product_detailed/productDetailed'
Vue.component('product-detailed', productDetailed);
